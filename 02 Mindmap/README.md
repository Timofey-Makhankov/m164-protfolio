# Mind-Map mit PlantUML

[TOC]

Diagram:

``` plantuml
@startmindmap
*[#EEE] DBMS
**[#EEE] Datenbankmanagmentsystem (DBMS)
***[#EEE] DBS
***[#EEE] Datenbank (DB)
--[#EEE] Relationale Datenbanksystem
--[#EEE] Integrierte Datenhaltung
**[#EEE] Sprache
***_ retrieval
***_ DML
***_ DDL
***_ DCL
--[#EEE] SQL
---_ MySQL
---_ PostgresQL
**[#EEE] Katalog
***_ Benutzersichten
***_ Konsistenzkontrolle
***_ Datenzugriffskontrolle
***_ Transaktionen
***_ Mehrbenutzerfähigkeit
***_ Datensicherung
**[#90EE90] Vorteile
***[#EEE] Grosse Wirtschaftlichkeit
****_ Kürzere Softwareentwicklungszeiten
***_ Nutzung von Standards
***_ Effizienter Datenzugriff
***_ Hohe Flexibilität & Verfügbarkeit
--[#FFCCCB] Nachteile
---_ hohe Anfangsinvestiotionen
---_ nicht speziallisiert
---_ Hochqualifiziertes Personal erforderlich
---_ gleichzeitiger Zugriff <b>nicht</b> möglich
@endmindmap
```

## Fortgeschritene Aufgaben

### Tablespace

Ein `Tablespace` ist ein Ort, wo die Datenbanken Daten wie Schemas oder Datenbank Dateien (InooDB) gespeichert werden. Es wird auch mit organisierung von Datenbanken benutzt. Ein vergleich zu ein Schema in PostgreSQL. Es hat verschiedene Arten von '**Tablespace**' in MariaDB.

- System Tablespace
- File-per-Table Tablespace
- Temporar Tablespace

Bei [`System Tablespace`] ist das "Default", wenn man ein Tablespace will erstellen. Man kann die Tablespaces eine gewisse Grösse angeben. Man kann aber die Grösse später verändern. Man kann auch die InnoDB Dateien auf `Raw-Partition` speichern.

Bei [`File-per-Table Tablespace`] werden jede erstellte Tabelle in MariaDB auf seinen eigene Tablespace gespeichert. bei System Tablespace werden alle Tables in einem Tablespace gespeichert.

Bei [`Temporar Tablespace`] Wird wie bei einer Normalen Tablespace erstellt. Der Unterschied ist aber, wenn der Server stoppt, werden die Temporare Tablespaces gelöscht

In MariaDB kann ein Tablespace encrypted, um schlechte Aktoren kein zugriff geben zu sensitiven Daten (E-Mail, AHV-Nummer oder Passwort).

### Partition

Man kann die Tabellen in MariaDB in kleinere stücke zerlegen und in einem Partition speichern.

Einige Gründe, um eine Tabelle zu Partition:

- Um grossen Tabellen verschnellern
- Man kann die Tabellen in verschiedene Speichermedien einspeichern. Archive Tabellen kann man in HDD's speichern und wichtige Daten in SSD's oder auf einem NVME Laufwerk
- Man muss nicht ein Voll-Backup erstellen. Nur wichtige Daten ein Backup machen

### Was macht eine storage engine in einer Datenbank?

Ein Storage Engine ist ein software für MariaDB, um gewisse Arbeitsbelastungen speziallisieren und optimierend arbeiten. Es gibt viele verschiedene Storage Engines für MariaDB:

- **InnoDB** oder **MariaDB 10.2** für allgemeine bearbeitung von Daten
- **Spider** oder **Galera** für scalierbar und paritioned Daten in der Datenbank
- **MyRocks** für Compressierte Daten und **Archive** für Archivedaten
- **CONNECT** oder **FederatedX** für Daten verbindung ausserhalb der MariaDB Datenbank
- **SphinxSE** für Daten durchsuchung und Abfragung

Es gibt noch viele Mehr für MariaDB

## Quellen

- [MariaDB Tablespaces](https://mariadb.com/kb/en/innodb-tablespaces/)
- [MariaDB Paritioning](https://mariadb.com/kb/en/partitioning-overview/)
- [MariaDB Storage Engine](https://mariadb.com/docs/server/storage-engines/)
- [MariaDB Storage Engine typen](https://mariadb.com/kb/en/choosing-the-right-storage-engine/)
