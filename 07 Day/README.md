# Tag 7

## Subquery Aufgaben

```sql
-- 1
SELECT titel FROM buecher
WHERE verkaufspreis IN (SELECT MAX(verkaufspreis) FROM buecher);

-- 2
SELECT titel FROM buecher
WHERE verkaufspreis IN (SELECT MIN(verkaufspreis) FROM buecher);

-- 3
SELECT * FROM buecher
WHERE einkaufspreis > (SELECT AVG(einkaufspreis) FROM buecher);

-- 4
SELECT * FROM buecher
WHERE einkaufspreis > (
SELECT AVG(einkaufspreis) FROM buecher b
JOIN buecher_has_sparten bhs ON bhs.buecher_buecher_id = b.buecher_id
JOIN sparten s ON bhs.sparten_sparten_id = s.sparten_id
WHERE s.bezeichnung = 'Thriller'
);

-- 5
SELECT * FROM buecher b
JOIN buecher_has_sparten bhs ON bhs.buecher_buecher_id = b.buecher_id
JOIN sparten s ON bhs.sparten_sparten_id = s.sparten_id
WHERE einkaufspreis > (
SELECT AVG(einkaufspreis) FROM buecher b
JOIN buecher_has_sparten bhs ON bhs.buecher_buecher_id = b.buecher_id
JOIN sparten s ON bhs.sparten_sparten_id = s.sparten_id
WHERE s.bezeichnung = 'Thriller'
) AND s.bezeichnung = 'Thriller';

-- 6
SELECT * FROM buecher
WHERE verkaufspreis - einkaufspreis > (
SELECT AVG(verkaufspreis - einkaufspreis) FROM buecher
WHERE buecher_id != 22
);

-- 7
SELECT SUM(derivedTable.average) FROM (SELECT s.bezeichnung, AVG(b.einkaufspreis) AS average FROM buecher b
JOIN buecher_has_sparten bhs ON bhs.buecher_buecher_id = b.buecher_id
JOIN sparten s ON bhs.sparten_sparten_id = s.sparten_id
WHERE s.bezeichnung != 'Humor'
GROUP BY s.bezeichnung) AS derivedTable;

-- 8
SELECT COUNT(derived_table.vorname) FROM (
SELECT vorname, nachname, COUNT(b.buecher_id) AS anzahl FROM autoren a
JOIN autoren_has_buecher ahb ON ahb.autoren_autoren_id = a.autoren_id
JOIN buecher b ON ahb.buecher_buecher_id = b.buecher_id
GROUP BY a.autoren_id
) AS derived_table
WHERE derived_table.anzahl > 4;

-- 9
SELECT v.name, AVG(b.verkaufspreis - b.einkaufspreis) AS durchschnitt FROM buecher b
JOIN verlage v ON v.verlage_id = b.verlage_verlage_id
GROUP BY v.verlage_id
HAVING AVG(b.verkaufspreis - b.einkaufspreis) < 10;
```
