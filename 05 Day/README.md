# Day 5

## Referentielle Datenintegrität

### Aufgabe 1

1. Wenn man versucht, ein Datensatz zu löschen, kann es dazu kommen, dass Datensätze, die mit der gelöschten Datensatz eine verbindung hatten, zu Integritäts problemen kommen kann. Darum werden die Datensätze nicht gelöscht, sondern deaktiviert oder ungreifbar.
2. Die referentielle integrität stellt die DBMS sicher.

### Aufgabe 2

1. Die Datensätze, die eine Beziehung zu Basel ist jetzt leer und man weiss nicht mehr, zu welchem Ort sie zugehören
2. Ich würde den Datensatz aktulisieren, wenn das nicht möglich wäre, erstelle ich ein neuen Datensatz, verändere alle Datensätze von Basel zum neunen und dann lösche ich den Datensatz

## Select alias

```sql
-- 1
SELECT kunde_id, name, fk_ort_postleitzahl FROM kunden AS kundenliste
WHERE kundenliste.fk_ort_postleitzahl > 80000;

-- 2
SELECT o.name,k.name FROM kunden k
INNER JOIN orte o 
ON id_postleitzahl = fk_ort_postleitzahl
WHERE o.name LIKE '%n';

-- 3
SELECT kunde_id, hrgs.name, prfz.name FROM kunden AS hrgs 
INNER JOIN orte AS prfz ON id_postleitzahl = fk_ort_postleitzahl 
ORDER BY hrgs.kunde_id;

-- 4
SELECT k.name, o.id_postleitzahl, o.name FROM kunden k
INNER JOIN orte o ON k.fk_ort_postleitzahl = o.id_postleitzahl
WHERE k.name LIKE '%a%' 
AND o.name LIKE '%u%';
```

## Aggregatsfunktionen

```sql
-- a
SELECT MAX(gehalt) FROM lehrer;

-- b
SELECT MIN(gehalt) FROM lehrer JOIN lehrer_hat_faecher WHERE idFaecher = 2;

-- c
SELECT AVG(noteMathe), AVG(noteDeutsch) FROM schueler;

-- d
SELECT MAX(einwohnerzahl) AS 'Höchste Einwohnerzahl', 
MIN(einwohnerzahl) AS 'Niedrigste Einwohnerzahl' FROM orte;

-- e
SELECT (MAX(einwohnerzahl) - MIN(einwohnerzahl)) AS 'Differenz' FROM orte;

-- f
SELECT COUNT(*) FROM schueler;

-- g
SELECT COUNT(*) FROM schueler 
WHERE idSmartphones IS NOT NULL;

-- h
SELECT COUNT(*) FROM schueler sch 
JOIN smartphones s ON s.id = sch.idSmartphones 
WHERE idSmartphones IS NOT NULL AND 
(marke LIKE 'Samsung' OR marke LIKE 'HTC');

-- i
SELECT COUNT(*) FROM schueler s 
JOIN orte o ON o.id = s.idOrte 
WHERE o.name LIKE '%Waldkirch%';

-- j
SELECT COUNT(*) FROM schueler s 
JOIN lehrer_hat_schueler lhs ON s.id = lhs.idSchueler 
WHERE s.idOrte = 1 AND idLehrer = 4;

-- k
SELECT COUNT(*) FROM schueler s 
JOIN lehrer_hat_schueler lhs ON s.id = lhs.idSchueler 
WHERE idLehrer = 28;

-- l
SELECT COUNT(*) FROM schueler s 
JOIN lehrer_hat_schueler lhs ON s.id = lhs.idSchueler 
WHERE idLehrer = 28 AND nationalitaet = 'RU';

-- m
SELECT MAX(gehalt) FROM lehrer;
```

## Select group by

```sql
-- a
SELECT COUNT(*), nationalitaet FROM schueler 
GROUP BY nationalitaet;

-- b
SELECT o.name AS 'Ort', COUNT(s.id) AS 'Anzahl der Schühler' FROM schueler s 
JOIN orte o ON o.id = s.idOrte 
GROUP BY s.idOrte 
ORDER BY 2 DESC;

-- c
SELECT f.fachbezeichnung AS 'Fachbezeichnung', COUNT(l.id) FROM lehrer l
JOIN lehrer_hat_faecher lhf ON l.id = lhf.idLehrer
JOIN faecher f ON f.id = lhf.idFaecher
GROUP BY f.id
ORDER BY 2;

-- d
SELECT f.fachbezeichnung AS 'Fachbezeichnung', l.name FROM lehrer l
JOIN lehrer_hat_faecher lhf ON l.id = lhf.idLehrer
JOIN faecher f ON f.id = lhf.idFaecher
GROUP BY f.id
ORDER BY 2;

-- e
SELECT s.name, l.name, f.fachbezeichnung FROM lehrer l
JOIN lehrer_hat_faecher lhf ON l.id = lhf.idLehrer
JOIN faecher f ON f.id = lhf.idFaecher
JOIN lehrer_hat_schueler lhs ON l.id = lhs.idLehrer
JOIN schueler s ON s.id = lhs.idSchueler
GROUP BY s.id
ORDER BY 2;
```

## Select having

```sql
-- 1
SELECT name AS 'Schüler', (AVG(noteMathe) + AVG(noteDeutsch)) / 2 AS 'Durchschnittsnote' 
FROM schueler 
GROUP BY name 
HAVING (AVG(noteMathe) + AVG(noteDeutsch)) / 2 > 4;

-- 2
SELECT name AS 'Lehrer', gehalt * 0.7 AS 'Nettogehalts' 
FROM lehrer 
WHERE gehalt * 0.7 > 3000;

-- 3
SELECT klassenzimmer, COUNT(id) 
FROM schueler 
GROUP BY klassenzimmer 
HAVING COUNT(id) < 10;

-- 4
-- a
SELECT COUNT(s.name), o.name FROM schueler s
JOIN orte o ON o.id = s.idOrte
WHERE nationalitaet = 'RU'
GROUP BY o.name
ORDER BY 2;

-- b
SELECT COUNT(s.name), o.name FROM schueler s
JOIN orte o ON o.id = s.idOrte
WHERE nationalitaet = 'RU'
GROUP BY o.name
HAVING COUNT(s.name) > 10
ORDER BY 2;
```

## Select group by order by

```sql
-- 1
SELECT * FROM nutztiere 
WHERE tierart LIKE "%Schafe%" AND jahr = 2018;

-- 2
SELECT gebiet_name, SUM(anzahl) 
FROM nutztiere 
WHERE tierart LIKE "%Schafe%" AND jahr = 2018 
GROUP BY gebiet_name 
ORDER BY 1;

-- 3
SELECT AVG(anzahl) 
FROM nutztiere 
WHERE tierart LIKE "%Kühe%" AND gebiet_name LIKE '%Zürich%';

-- 4
SELECT MAX(anzahl) 
FROM nutztiere 
WHERE tierart LIKE "%Kühe%" AND gebiet_name LIKE '%Zürich%';

-- 5
SELECT MIN(anzahl) 
FROM nutztiere 
WHERE tierart LIKE "%Kühe%" AND gebiet_name LIKE '%Zürich%';

-- 6
SELECT SUM(anzahl) 
FROM nutztiere 
WHERE jahr = 2016 
GROUP BY gebiet_name;

-- 7
SELECT SUM(anzahl) 
FROM nutztiere 
WHERE jahr = 2016 
GROUP BY gebiet_name, jahr;

-- 8
SELECT SUM(anzahl) 
FROM nutztiere 
WHERE jahr = 2016 
GROUP BY gebiet_name, jahr 
ORDER BY jahr;

-- 9
SELECT SUM(anzahl) 
FROM nutztiere 
WHERE jahr > 2015 
GROUP BY gebiet_name, jahr 
ORDER BY jahr;
```
