# Recap KEL

1. Welche Stufen gibt es bei der Wissenstreppe?

    Nennen Sie diese der Reihe nach und machen Sie ein Beispiel mit einem Wechselkurs.

   1. Buchstaben / Ziffern / Zahlen

      Irgendwelche Zeichen, kann auch ein Zeichensatz sein wie UTF-8 (z.B. p, G, 6, @, è)

   2. Daten

      Der Syntax, Grammatik und Wertigkeit von Zeichen geben (z.B. 135.35)

   3. Information

      Den Daten eine Bedeutung / Semantik geben (135.35 CHF)

   4. Wissen

      Die information eine Zusammenhang geben oder Erfahrung zu dieser Information geben (z.B. Total = 135.35 CHF)

   5. Kompetenz

      Mit Wissen eine Handlung oder einen praktischen Wert (progmatik) geben (z.B. Budgetplannung)

2. Wie werden Netzwerk-Beziehungen im logischen Modell abgebildet?

   Die Netzwerk-Beziehungen werden mit einem Transformations-Tabelle erstellt. Es werden beide Fremdschlüssel in den Transformations-Tabelle gegeben und verbunden. Der Grund dafür ist, das es soll nur ein Fremdschlüssel pro Beziehung haben.

3. Was sind Anomalien in einer Datenbasis? Welche Arten gibt es?

   Anomalien sind Fehler in der Datenstruktur

   - Löschanomalie

     Bei der Löschanomalie kann passieren, ein Datensatz zu löschen, kann bei einem anderen Datensatz in einer anderen Tabelle ein Problem erstellt werden.

   - Änderungsanomalie

     Etwas ähnliches wie die Löschanomalie, aber es wird ein Problem erstellt, wenn ein Datensatz verändert wird.

   - Einfügeanomalie

     Wie bei den anderen, aber wird es verursacht, wenn ein Datensatz in die Datenbank eingefügt wird.

4. Gibt es redundante "Daten"? Warum?

   Es kann Redundante Daten in einer Datenbank ankommen. Es kann passieren, wenn man bei mehreren Tabellen die Information einfügt. Kommt zu Doppelt abgespeicherte Informationen (z.B. Alter = Geburtsdatum)

5. Datenstrukturierung:

   Unstrukturiert -> Semi-strukturiert -> Strukturiert

   1. Welche zwei Aspekte können strukturiert werden?
  
   2. Welche Kategorien (Abstufungen) gibt es bei der Strukturierung?

   3. Und wie müssen die Daten in einer DB strukturiert sein?

6. Beschreiben das Bild mit den richtigen Fachbegriffen

   1. Tabellennamen: eindeutig
   2. Datensätze (Tupel) (Record) -> hat eine ID
   3. Spalten: Attribut (Column) -> Datentyp definiert, Domäne (Wertenbereich)
   4. Felder: auch NULL

   Entitättypen

7. Welche (einschränkende) Einstellungen zu den Attributen (z.B. ID) kennen Sie?

   Einschränkende Eigenschaften => Constraint

   - NOT NULL
   - UNIQUE
   - PK
   - FK
   - DEFAULT (standart Wert)
   - CHECK (wertebereich überprüft)
