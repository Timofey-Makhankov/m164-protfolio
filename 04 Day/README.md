# Day 4

## Aufgabe 14 DQL 05 SELECT

```sql
# a
SELECT * FROM dvd_sammlung;

# b
SELECT nummer, film FROM dvd_sammlung;

# c
SELECT regisseur, film FROM dvd_sammlung;

# d
SELECT film FROM dvd_sammlung 
WHERE regisseur LIKE 'Quentin tarantino';

# e
SELECT film FROM dvd_sammlung 
WHERE regisseur LIKE 'Steven Spielberg';

# f
SELECT film FROM dvd_sammlung 
WHERE regisseur LIKE 'Steven%';

# g
SELECT film FROM dvd_sammlung 
WHERE laenge_minuten > 120;

# h
SELECT film FROM dvd_sammlung 
WHERE regisseur LIKE '%Tarantino%' OR regisseur LIKE '%Spielberg%';

# i
SELECT film FROM dvd_sammlung 
WHERE regisseur LIKE '%Tarantino%' AND laenge_minuten < 90;

# j
SELECT film FROM dvd_sammlung 
WHERE film LIKE '%Sibirien%';

# k
SELECT * FROM dvd_sammlung 
WHERE film LIKE '%Das%%Rennen%';

# l
SELECT * FROM dvd_sammlung 
ORDER BY regisseur;

# m
SELECT * FROM dvd_sammlung 
ORDER BY regisseur, film;

# n
SELECT * FROM dvd_sammlung 
WHERE regisseur LIKE '%Tarantino%' 
ORDER BY laenge_minuten DESC;
```

## Contraint Syntax

```sql
CONSTRAINT 'name'
FOREIGN KEY ('FK_row')
REFERENCES 'table_name' ('PK_row')
```

## Fremdschlüssel NOT NULL

```sql
FK_row INT NOT NULL
```

Jeder Fremdschlüssel wird mit einem Index hergestellt, damit es mit der Abfrage schneller durchgeht (Abfrage Optimierung)

Der Contraint `UNIQUE` wird beim Indexierung erstellt

```sql
CREATE UNIQUE INDEX 'name_idx' ON 'table' ('row' ASC / DESC)
```

`UNIQUE` kann nur an Fremdschlüssel gegeben

```sql
ALTER TABLE <DetailTab>
  ADD CONSTRAINT <Constraint> FOREIGN KEY (<Fremdschlüssel>)
  REFERENCES <MasterTab> (Primärschlüssel);
```

```sql
ALTER TABLE <Tabelle>
  ADD UNIQUE (<Spalte>);
```

Bei der `1:C` Beziehung versuche ich ein index zu geben, dieser schon exsistiert, wird mir ein Error vorkommen.

## Mengenlehre

### Aufgabe 1

- A = {c, e, z, r, d, g, u, x}
- B = {c, e, g}
- C = {r, d, g, t}
- D = {e, z, u}
- E = {z, r, u}

- a -> B ⊂ A = true
- b -> C ⊂ A = false
- c -> E ⊂ A = true
- d -> B ⊂ C = false
- e -> E ⊂ C = false

### Aufgabe 2

- A = {1, 2, 3, 4, 5}
- B = {2, 5}
- C = {3, 5, 7, 9}

- a -> A ∩ B = {2, 5}
- b -> A ∪ C = {1, 2, 3, 4, 5, 7, 9}
- c -> B<sup>c</sup> = {1, 3, 4, 7, 9}
- d -> B/C = {2}
- e -> C/B = {3, 7, 9}

## Aufgabe DQL_JOIN

### 2

```sql
# a
SELECT k.name, postleitzahl, o.name FROM kunden k INNER JOIN orte o;

# b
SELECT k.name, o.name FROM kunden k INNER JOIN orte o WHERE postleitzahl = 79312;

# c
SELECT k.name, o.name FROM kunden k INNER JOIN orte o WHERE o.name LIKE '%Emmendingen%';

# d
SELECT k.name, o.name, einwohnerzahl FROM kunden k INNER JOIN orte o WHERE einwohnerzahl > 70000;

# e
SELECT DISTINCT o.name, postleitzahl, einwohnerzahl FROM kunden k INNER JOIN orte o WHERE einwohnerzahl < 1000000;

# f
SELECT k.name, o.name FROM kunden k INNER JOIN orte o WHERE einwohnerzahl > 100000 AND einwohnerzahl < 1500000;

# g
SELECT k.name, postleitzahl, o.name FROM kunden k INNER JOIN orte o WHERE k.name LIKE '%e%' AND (o.name LIKE '%u%' OR o.name LIKE '%r%');
```
