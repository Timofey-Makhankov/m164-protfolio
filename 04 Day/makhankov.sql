CREATE SCHEMA IF NOT EXISTS db_ubs;
USE db_ubs;

CREATE TABLE IF NOT EXISTS kunde_info(
    id_kunde INT NOT NULL AUTO_INCREMENT,
    nachname VARCHAR(50) NOT NULL,
    kontonummer VARCHAR(5) NOT NULL,
    swift INT,
    kontostand DECIMAL(10, 2),
    erstellt_am DATE,
    PRIMARY KEY (id_kunde)
);

INSERT INTO kunde_info VALUES
(NULL, 'makhankov', '99999', '69420', '11.45', '2015-03-04');

INSERT INTO kunde_info (nachname, kontonummer) VALUES
('trump', '66666');

ALTER TABLE kunde_info ADD COLUMN geeandert_am DATETIME;

UPDATE kunde_info
SET kontostand = (SELECT kontostand FROM kunde_info WHERE nachname LIKE 'makhankov') + 1000
WHERE nachname LIKE 'makhankov';

SELECT id_kunde, nachname, kontonummer, kontostand FROM kunde_info
WHERE (nachname LIKE 'Kel%' OR kontonummer > '3322') AND kontostand <= 100
ORDER BY nachname, kontostand DESC;