# Tag 6

## Datensicherung

```powershell
.\mysqldump.exe > D:\output.sql -u root --databases tourenplaner
```

Mit diesem Befehl kann ich ein Dumb auf die Schema 'tournerplaner' erstellen

1. Ihr dumpfile (Backup-File) ist nun nicht mehr leer. Analysieren Sie den Inhalt.
   - Was finden Sie vor? - Es it mit SQL Befehlen die ganze Schema erstellt
   - Beschreiben Sie ihn. - besteht aus SQL Befehlen

a - Wir haben ein logisches Backup erstellt

b - es ist langsamer und hat keine log und configuration Dateien

Was ist der Unterschied zwischen online- und offline Backups? - **bei einer online Backup ist der SQL Server am laufen und ein Backup am erstellen. Bei einem offline Backup macht man ein Backup wenn der SQL Server nicht laufen ist**

Was ist ein "snapshot Backup?" - **Bei einer Snapshot Backup erstellt man ein Backup in einer gewissen Zeit und wird nicht die ganze Datenbank ein Backup erstellen. Nur gewisse veränderungen**

## HR Aufgaben

### Teilaufgabe 1

Etwas ist bei der Entwicklung des Entity Relationship Diagram's schiefgelaufen - **Die Beziehungen zwischen den Tabellen sind verloren. Die Gefahr kommt, wenn man es nicht aktualisiert ist, man weiss nicht, welche FK zu PK gehören und was für beziehungen sie haben.**

```sql
-- 1
SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME, SALARY 
FROM employees; 

-- 2
SELECT MAX(SALARY), MIN(SALARY) 
FROM employees;

-- 3
SELECT COUNT(*) 
FROM employees;

-- 4
SELECT * 
FROM employees 
WHERE LAST_NAME LIKE 'R%s';

-- 5
SELECT * 
FROM employees 
WHERE COMMISSION_PCT IS NULL;

-- 6
SELECT FIRST_NAME, LAST_NAME, SALARY 
FROM employees 
WHERE SALARY NOT BETWEEN 10000 AND 15000;

-- 7
SELECT FIRST_NAME, LAST_NAME, DEPARTMENT_ID 
FROM employees 
WHERE DEPARTMENT_ID = 30 OR DEPARTMENT_ID = 100 
ORDER BY LAST_NAME ASC;

-- 8
SELECT FIRST_NAME, LAST_NAME, SALARY 
FROM employees 
WHERE 
    (SALARY NOT BETWEEN 10000 AND 15000) 
    AND 
    (DEPARTMENT_ID = 30 OR DEPARTMENT_ID = 100);

-- 9
SELECT FIRST_NAME, LAST_NAME, HIRE_DATE 
FROM employees 
WHERE YEAR(HIRE_DATE) = 1987;

-- 10
SELECT FIRST_NAME 
FROM employees 
WHERE FIRST_NAME LIKE '%c%' OR FIRST_NAME LIKE '%b%';

-- 11
SELECT LAST_NAME, JOB_TITLE, SALARY 
FROM employees e
JOIN jobs j ON e.JOB_ID = j.JOB_ID
WHERE 
    (JOB_TITLE LIKE '%programmer%' OR JOB_TITLE LIKE '%shipping%') 
AND
    (SALARY != 4500 OR SALARY != 10000 OR SALARY != 15000);

-- 12
SELECT LAST_NAME 
FROM employees 
WHERE LAST_NAME LIKE '__e%';

-- 13 ???

-- 14
SELECT 
    FIRST_NAME, 
    LAST_NAME, 
    SALARY, 
    (SALARY * 1.15) AS 'Gewinnbeteiligung' 
FROM employees;


-- 15
SELECT * FROM employees WHERE 
LAST_NAME LIKE '%jones%' OR
LAST_NAME LIKE '%blake%' OR
LAST_NAME LIKE '%scott%' OR
LAST_NAME LIKE '%king%' OR
LAST_NAME LIKE '%ford%';

-- 16
SELECT FIRST_NAME, LAST_NAME, JOB_TITLE, e.DEPARTMENT_ID 
FROM employees e
JOIN jobs j ON e.JOB_ID = j.JOB_ID
JOIN departments d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID
JOIN locations l ON d.LOCATION_ID = l.LOCATION_ID
WHERE l.CITY LIKE '%London%';

-- 17
SELECT e.EMPLOYEE_ID, e.LAST_NAME, m.MANAGER_ID, m.LAST_NAME 
FROM employees e
JOIN employees m ON e.MANAGER_ID = m.EMPLOYEE_ID;

-- 18
SELECT FIRST_NAME, LAST_NAME, HIRE_DATE 
FROM employees
WHERE HIRE_DATE >= (
    SELECT HIRE_DATE FROM employees WHERE LAST_NAME LIKE '%Jones%'
);

-- 19

-- 20
-- 21
-- 22
-- 23
-- 24
-- 25
```
