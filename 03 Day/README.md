# Tag 3

[TOC]

## Aufgabe 10_DDL 05 Alter table

```sql
USE test;

# Tabelle erstellen
CREATE TABLE employees (
    id INT AUTO_INCREMENT,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    email VARCHAR(100),
    hire_date DATE,
    salary DECIMAL (10,2),
    PRIMARY KEY (id) 
    );

# Neue Spalte erstellen
ALTER TABLE employees ADD department VARCHAR(50);

# Der Datentyp verändern
ALTER TABLE employees MODIFY salary FLOAT(10, 2);

# Die Spalte verändern
ALTER TABLE employees CHANGE department id_department INT;

# Die Spalte löschen
ALTER TABLE employees DROP email;
```

## Aufgabe 12_DML 05 Insert

```sql
USE test;
CREATE TABLE kunden (
    kunde_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    vorname VARCHAR(255), 
    nachname VARCHAR(255), 
    land_id INT, 
    wohnort VARCHAR(255)
);

# a
INSERT INTO kunden VALUES
(NULL, 'Heinrich', 'Schmitt', '2', 'Zürich');

# b
INSERT INTO kunden VALUES
(NULL, 'Sabine', 'Müller', '2', 'Bern')

# c
INSERT INTO kunden VALUES
(NULL, 'Markus', 'Musertmann', '1', 'Wien')

# d
INSERT INTO kunden
    (nachname)
VALUE
    ('Maier');

# e
INSERT INTO kunden
    (nachname, wohnort)
VALUES
    ('Bulgut', 'Sirnach');

# f
INSERT INTO kunden
    (vorname, nachname)
VALUES
    ('Maria', 'Manta');
```

### Aufgabe 2

```sql
# a - Es fählt die Tabellen Namen
INSERT INTO kunden (nachname, wohnort, land_id) VALUES
('Fesenkampp', 'Duis-burg', 3);

# b - die Spalten Namen soll keine einfache Anführungszeichen haben
INSERT INTO kunden (vorname) VALUES ('Herbert');

# c - Das land_id muss eine Zahl sein
INSERT INTO kunden (nachname, vorname, wohnort, land_id) VALUES
('Schulter', 'Albert', 'Duisburg', '3');

# d - Das Schlüsselwort VALUES fehlt
INSERT INTO kunden VALUES
(NULL, 'Brunhild', 'Sulcher', 1, 'Süderstade');

# e - bei kurzem Syntax müssen alle Werte gegeben, auch bei AUTO_INCREMENT
INSERT INTO kunden VALUES 
(NULL, 'Jochen', 'Schmied', 2, 'Solingen');

# f - Alle Werte müssen gegeben werden, wenn nicht, dann mit NULL angeben
INSERT INTO kunden VALUES 
(NULL, '', 'Doppelbrecher', 2, '');

# g - die Spaltennamen entsprechen nicht die Werte anzahl
INSERT INTO kunden (vorname, nachname, wohnort, land_id) VALUES 
('Christoph', 'Fesenkampp', 'Duisburg', 3);

# h - Die Abfrage funktioniert

# i - die Werte müssen mit einfache Anführungszeichen beschriftet
INSERT INTO kunden (nachname, vorname, wohnort, land_id) VALUES 
('Schulter', 'Albert', 'Duisburg', '1');

# j - Die Werte müssen mit einfache Anführungszeichen beschriftet werden
INSERT INTO kunden VALUE 
(NULL, 'Brunhild', 'Sulcher', 1, 'Süderstade');

# k - der letzte Wert fällt das einfache Anführungszeichen
INSERT INTO kunden VALUE ('', 'Jochen', 'Schmied', 2, 'Solingen');

```

## Aufgabe 12_DML Update, delete alter und drop

```sql
# 1
UPDATE dvd_sammlung
SET regisseur = 'Etan Cohen'
WHERE regisseur = 'Cohen';

# 2
UPDATE dvd_sammlung
SET laenge_minuten = '120'
WHERE nummer = '2';

# 3
RENAME TABLE dvd_sammlung TO bluray_sammlung;

# 4
ALTER TABLE bluray_sammlung ADD COLUMN preis FLOAT(10, 2);

# 5
DELETE FROM bluray_sammlung
WHERE film = 'Angriff auf Rom';

# 6
ALTER TABLE filmedatenbank.bluray_sammlung 
CHANGE film kinofilm VARCHAR(255);

# 7
ALTER TABLE bluray_sammlung
DROP COLUMN nummer;

# 8
DROP TABLE bluray_sammlung;
```
